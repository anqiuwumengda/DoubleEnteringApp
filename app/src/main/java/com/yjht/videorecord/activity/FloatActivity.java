package com.yjht.videorecord.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.yjht.videorecord.R;
import com.yjht.videorecord.util.IDCard;

import java.text.ParseException;

public class FloatActivity extends Activity {
    public static final int RECORD_CUSTOM_VIDEO = 2;
    private EditText edName, edIdNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.float_activity);
        initView();
    }
    @Override
    protected void onResume() {
        super.onResume();
        edName.setText("");
        //edIdNo.setText("");
    }

    private void initView() {
        edName = (EditText) findViewById(R.id.ed_name);
        edIdNo = (EditText) findViewById(R.id.ed_idNo);
        //edIdNo.setInputType(InputType.TYPE_CLASS_NUMBER);
        //限制输入类型
        edIdNo.setKeyListener(DigitsKeyListener.getInstance("1234567890X"));
    }
    public void btnOnclick(View view){
        validateInput();
    }
    private boolean validateInput() {
        String name = edName.getText().toString();
        String idNo = edIdNo.getText().toString();
        boolean flag = false;
        if(name.equals("")){
            Toast.makeText(this,R.string.tip_inputName,Toast.LENGTH_SHORT).show();
        }
        if(idNo.equals("")){
            Toast.makeText(this,R.string.tip_inputIdNo,Toast.LENGTH_SHORT).show();
        }
        if(!name.equals("") && !idNo.equals("")){
            try {
                flag = IDCard.IDCardValidate(idNo);
                if(flag == true){
                    Intent intent = new Intent(this, CustomRecordActivity.class);
                    intent.putExtra("name",name);
                    intent.putExtra("idNo",idNo);
                    startActivityForResult(intent, RECORD_CUSTOM_VIDEO);
                    finish();
                }else {
                    Toast.makeText(this,R.string.invalidIdNo,Toast.LENGTH_SHORT).show();
                }
                return flag;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
