package com.yjht.videorecord.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.yjht.videorecord.R;
import com.yjht.videorecord.activity.FloatActivity;


public class FirstFragment extends Fragment {
    private TextView textView;
    private Context context;
    private Button button;
    public FirstFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        initView(view);
        showActivity();
        return view;
    }

    private void initView(View view) {
        context = getActivity();
        textView = (TextView) view.findViewById(R.id.textView_fistActivity);
        button = (Button) view.findViewById(R.id.btnStart);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,FloatActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showActivity() {
        textView.setText(R.string.app_introduce);
        textView.setTextSize(20);
    }






}
